<?php

namespace Officient\DataCollector;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DataCollectorBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\DataCollector
 */
class DataCollectorBundle extends Bundle
{
    //
}