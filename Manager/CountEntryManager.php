<?php

namespace Officient\DataCollector\Manager;

use DateTimeInterface;
use Exception;
use Officient\DataCollector\CountCollection;
use Officient\DataCollector\Entity\CountEntry;

class CountEntryManager extends AbstractManager implements CountEntryManagerInterface
{
    /**
     * @inheritDoc
     */
    public function findAll(?string $ownerPrefix = null, ?string $ownerId = null, ?DateTimeInterface $from = null, ?DateTimeInterface $to = null, ?string $serviceTag = null): CountCollection
    {
        $query = '/count_entries';
        $params = array();
        if($ownerPrefix) {
            $params[] = "ownerPrefix=$ownerPrefix";
        }
        if($ownerId) {
            $params[] = "ownerId=$ownerId";
        }
        if($from && $to) {
            $from = $from->format('YmdHis');
            $to = $to->format('YmdHis');
            $params[] = "fromDatetime=$from&toDatetime=$to";
        }
        if($serviceTag) {
            $params[] = "serviceTag=$serviceTag";
        }
        if(!empty($params)) {
            $query .= "?".implode("&", $params);
        }

        $result = array();
        $sum = 0;
        $response = $this->client->doRequest($query);
        if($response->getHttpCode() === 200 && is_array($response->getContent()) && isset($response->getContent()['data']) && isset($response->getContent()['sum'])) {
            $sum = $response->getContent()['sum'];
            foreach ($response->getContent()['data'] as $value) {
                $result[] = $this->createCountEntry($value);
            }
        }

        return new CountCollection($sum, $result);
    }

    /**
     * @param array $value
     * @return CountEntry
     * @throws Exception
     */
    private function createCountEntry(array $value)
    {
        return (new CountEntry())
            ->setId($value['id'])
            ->setOwnerPrefix($value['ownerPrefix'])
            ->setOwnerId($value['ownerId'])
            ->setServiceTag($value['serviceTag'])
            ->setCount($value['count'])
            ->setCollectedDate(new \DateTime($value['collectedDate']))
            ->setUpdatedDatetime(new \DateTime($value['updatedDatetime']))
            ->setCreatedDatetime(new \DateTime($value['createdDatetime']));
    }
}