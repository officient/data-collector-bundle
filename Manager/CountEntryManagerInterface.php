<?php

namespace Officient\DataCollector\Manager;

use DateTimeInterface;
use Officient\DataCollector\CountCollection;

interface CountEntryManagerInterface
{
    /**
     * @param string|null $ownerPrefix
     * @param string|null $ownerId
     * @param DateTimeInterface|null $from
     * @param DateTimeInterface|null $to
     * @param string|null $serviceTag
     * @return CountCollection
     */
    public function findAll(?string $ownerPrefix = null, ?string $ownerId = null, ?DateTimeInterface $from = null, ?DateTimeInterface $to = null, ?string $serviceTag = null): CountCollection;
}