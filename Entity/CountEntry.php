<?php

namespace Officient\DataCollector\Entity;

use DateTimeInterface;

class CountEntry implements \JsonSerializable
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $ownerPrefix;

    /**
     * @var int
     */
    protected $ownerId;

    /**
     * @var string|null
     */
    protected $serviceTag;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var DateTimeInterface
     */
    protected $collectedDate;

    /**
     * @var DateTimeInterface
     */
    protected $updatedDatetime;

    /**
     * @var DateTimeInterface
     */
    protected $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'ownerPrefix' => $this->ownerPrefix,
            'ownerId' => $this->ownerId,
            'serviceTag' => $this->serviceTag,
            'count' => $this->count,
            'collectedDate' => $this->collectedDate,
            'updatedDatetime' => $this->updatedDatetime,
            'createdDatetime' => $this->createdDatetime
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CountEntry
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnerPrefix(): string
    {
        return $this->ownerPrefix;
    }

    /**
     * @param string $ownerPrefix
     * @return CountEntry
     */
    public function setOwnerPrefix(string $ownerPrefix): self
    {
        $this->ownerPrefix = $ownerPrefix;
        return $this;
    }

    /**
     * @return int
     */
    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    /**
     * @param int $ownerId
     * @return CountEntry
     */
    public function setOwnerId(int $ownerId): self
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getServiceTag(): ?string
    {
        return $this->serviceTag;
    }

    /**
     * @param string|null $serviceTag
     * @return CountEntry
     */
    public function setServiceTag(?string $serviceTag): self
    {
        $this->serviceTag = $serviceTag;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return CountEntry
     */
    public function setCount(int $count): self
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCollectedDate(): DateTimeInterface
    {
        return $this->collectedDate;
    }

    /**
     * @param DateTimeInterface $collectedDate
     * @return CountEntry
     */
    public function setCollectedDate(DateTimeInterface $collectedDate): self
    {
        $this->collectedDate = $collectedDate;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedDatetime(): DateTimeInterface
    {
        return $this->updatedDatetime;
    }

    /**
     * @param DateTimeInterface $updatedDatetime
     * @return CountEntry
     */
    public function setUpdatedDatetime(DateTimeInterface $updatedDatetime): self
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedDatetime(): DateTimeInterface
    {
        return $this->createdDatetime;
    }

    /**
     * @param DateTimeInterface $createdDatetime
     * @return CountEntry
     */
    public function setCreatedDatetime(DateTimeInterface $createdDatetime): self
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}