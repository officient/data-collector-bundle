<?php

namespace Officient\DataCollector;

class CountCollection extends Collection
{
    /**
     * @var int
     */
    protected $sum;

    /**
     * @param int $sum
     * @param array $items
     */
    public function __construct(int $sum, array $items = array())
    {
        parent::__construct($items);
        $this->sum = $sum;
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }
}